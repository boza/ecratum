Rails.application.routes.draw do

  mount ActionCable.server => '/cable'

  resources :users, only: [:create]
  resources :messages, only: [:create]

  root to: 'room#index'

end
