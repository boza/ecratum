require 'test_helper'

class MessagesControllerTest < ActionDispatch::IntegrationTest
  fixtures :users

  def setup
    cookies[:user] = users(:one).id
    User.expects(:find).returns(users(:one))
  end

  test "#create" do

    Translator.expects(:new).with(dialect: 'pirate').returns(stub(:say))

    post messages_url, params: { body: 'Test' }

    assert_response :ok

  end

end
