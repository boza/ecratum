require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  fixtures :users

  test "#POST create success" do
    post users_url, params: { login: "login", dialect: "yoda" }
    assert cookies['user'].present?
    assert_equal 201, response.status
  end

  test "#POST create failed login param missing" do
    assert_raise ActionController::ParameterMissing do
      post users_url, params: { dialect: "yoda" }
    end
  end

  test "#POST create failed dialect param missing" do
    assert_raise ActionController::ParameterMissing do
      post users_url, params: { login: "yoda" }
    end
  end

  test "#POST create failed user invalid" do
    post users_url, params: { login: users(:one).login, dialect: "yoda" }
    expected_body = { "login" => ["has already been taken"] }

    assert_equal expected_body, JSON.parse(response.body)
    assert_equal 401, response.status
  end

end
