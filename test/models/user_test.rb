require 'test_helper'

class UserTest < ActiveSupport::TestCase
  fixtures :users

  test "login uniqueness" do
    user = User.new(login: users(:one).login)

    refute user.valid?
    assert "has already been taken", user.errors[:login]
  end

  test "login presence validation" do
    user = User.new

    refute user.valid?
    assert "can't be blank", user.errors[:login]
  end

  test "login dialect presence" do
    user = User.new(login: 'login')

    refute user.valid?
    assert "can't be blank", user.errors[:dialect]
  end

  private

end
