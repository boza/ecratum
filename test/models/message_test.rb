require 'test_helper'

class MessageTest < ActiveSupport::TestCase
  fixtures :users

  def setup
    @message = Message.new
  end

  test "validates user presence" do
    refute @message.valid?
    assert_includes @message.errors[:user], "can't be blank"
  end

  test "validates user existence" do
    refute @message.valid?
    assert_includes @message.errors[:user], "must exist"
  end

  test "validates body presence" do
    refute @message.valid?
    assert_includes @message.errors[:body], "can't be blank"
  end

  test "message is translated to user's dialect" do
    user = users(:one)
    stub = mock(say: "Translated message")
    Translator.expects(:new).with(dialect: user.dialect).returns(stub)
    @message.user = user
    @message.body = "This is a message"

    assert_equal "Translated message", @message.translated_text
  end

end
