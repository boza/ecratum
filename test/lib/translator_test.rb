require 'test_helper'

class TranslatorTest < ActiveSupport::TestCase


  test "dialect cannot be an empty string" do
    assert_raise Translator::NilDialect do
      Translator.new(dialect: '')
    end
  end

  test "dialect cannot be nil" do
    assert_raise Translator::NilDialect do
      Translator.new(dialect: nil)
    end
  end

  test "say success" do
    stub_request(:post, "http://api.funtranslations.com/translate/yoda.json").
      with(:body => "text=This%20are%20my%20last%20words",
           :headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
      to_return(status: 200, body: {contents: { translated: "My last words this are" }}.to_json, headers: {  "Content-Type" => "application/json" })

    assert_match "My last words this are", Translator.new(dialect: 'yoda').say("This are my last words")
  end

  test "say dialect not found" do
    stub_request(:post, "http://api.funtranslations.com/translate/not_known.json").
      with(:body => "text=This%20are%20my%20last%20words",
           :headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
      to_return(status: 404, body: {contents: { translated: "My last words this are" }}.to_json, headers: {  "Content-Type" => "application/json" })

    assert_raise Translator::UnknownDialect do
      Translator.new(dialect: 'not_known').say("This are my last words")
    end
  end

end
