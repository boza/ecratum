class AddTranslatedTextToMessages < ActiveRecord::Migration[5.0]
  def change
    add_column :messages, :translated_text, :text
  end
end
