class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :login, index: { unique: true }
      t.integer :dialect

      t.timestamps
    end
  end
end
