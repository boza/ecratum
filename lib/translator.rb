require 'httparty'

class Translator

  class UnknownDialect < StandardError;end
  class NilDialect < StandardError;end

  attr_reader :dialect

  include HTTParty
  base_uri "http://api.funtranslations.com/translate"
  raise_on (400..599).to_a

  def initialize(dialect: nil)
    raise NilDialect.new("Please tell me which dialect you want to translate") if dialect.nil? || dialect.strip !~ /\S/
    @dialect = dialect.strip
  end

  def say(text)
    self.class.post("/#{dialect}.json", body: { text: text })
      .parsed_response
      .fetch('contents', {})['translated']
  rescue HTTParty::Error => exception
    raise UnknownDialect.new("Sorry I don't speak #{dialect}") if exception.response.is_a?(Net::HTTPNotFound)
    raise exception
  end

end
