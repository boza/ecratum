# Chatroom

Have conversations with people using funny dialects!

## Installation

* Clone repository
* Bundle install
* Start server (`rails s`)

## Caveats

Since we are using the free `http://funtranslations.com/api` to translate the text we only get a few requests per hour, after that the text  wont be able to be translated

## Posible improvements

* Message status (who read your message)
* Add even more test coverage
* Move action cable broadcast to sidekiq worker

