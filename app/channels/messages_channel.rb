class MessagesChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'messages' do |payload|
      parsed_payload = JSON.parse(payload)
      message = Message.find(parsed_payload['message'])
      parsed_payload.merge!(template: MessagesController.new.render_to_string(partial: 'message', formats: [:html], locals: { message: message, current_user: current_user }))
      transmit parsed_payload
    end
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
