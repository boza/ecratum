$('[data-toggle="tooltip"]').tooltip()

bindMessageForm = () ->
  $('#send-message').click (e) ->
    e.preventDefault()
    body = $('#body').val()

    if body is ''
      alert('Message is empty')
      return false

    data = { body: $('#body').val() }
    $.post('/messages', data).fail(failedMessage).complete(() -> $('#body').val(''))

successLogin = () ->
  template = """
    <div class="message_input_wrapper">
      <input id="body" class="message_input" placeholder="Type your message here..." />
    </div>
    <div class="send_message">
      <div class="icon"></div>
      <div class="text" id="send-message">Send</div>
    </div>
  """

  $('.bottom_wrapper').html(template)
  bindMessageForm()

failedLogin = (data) ->
  alert('Something went wrong')

failedMessage = (data) ->
  alert('Something went wrong')

$('.messages').animate({ scrollTop: $('.messages').prop('scrollHeight') }, 0);

$('#loginButton').click (e)->
  e.preventDefault()
  data = { login: $('#login').val(), dialect: $('#dialect').val() }
  $.post('/users', data, successLogin).fail(failedLogin)

bindMessageForm()

