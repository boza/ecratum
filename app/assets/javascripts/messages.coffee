App.messages = App.cable.subscriptions.create('MessagesChannel',
  received: (data) ->
    $message = $(data.template)
    $messages = $('.messages')
    $messages.append $message
    setTimeout (->
      $message.addClass 'appeared'
    ), 0
    $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
)
