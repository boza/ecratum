class Message < ApplicationRecord
  belongs_to :user

  validates :body, presence: true
  validates :user, presence: true

  delegate :dialect, to: :user

  def body=(text)
    self[:body] = text
    translate_to_dialect(text)
  end

  def position(viewing_user)
    viewing_user == user ? 'right' : 'left'
  end

  private

  def translate_to_dialect(text)
    return unless text
    self[:translated_text] = Translator.new(dialect: user.dialect).say(text)
  rescue
    logger.error "Unable to translate message's body"
  end

end
