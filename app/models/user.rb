class User < ApplicationRecord

  enum dialect: [:yoda, :pirate, :binary]

  validates :login, presence: true, uniqueness: true
  validates :dialect, presence: true

  has_many :messages, dependent: :destroy

end
