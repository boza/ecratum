class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper_method :current_user

  rescue_from ActiveRecord::RecordInvalid do |exception|
    render json: exception.record.errors, status: 401
  end

  def current_user
    return if cookies[:user].blank?
    @current_user = User.find(cookies.signed[:user])
  end

end
