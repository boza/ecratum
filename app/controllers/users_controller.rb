class UsersController < ApplicationController

  def create
    login = params.require('login')
    dialect = params.require('dialect')
    user = User.create!(login: login, dialect: dialect)
    cookies.signed[:user] = user.id
    head :created
  end

end
