class RoomController < ApplicationController

  def index
    @messages = Message.includes(:user).last(30)
  end

end
