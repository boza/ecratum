class MessagesController < ApplicationController

  def create
    @message = Message.create!(user: current_user, body: params.require('body'))
    ActionCable.server.broadcast('messages', { message: @message.id })
    head :ok
  end

end
